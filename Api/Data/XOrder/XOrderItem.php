<?php
/**
 * Created by mr.vjcspy@gmail.com - khoild@smartosc.com.
 * Date: 07/01/2017
 * Time: 16:20
 */

namespace SM\Core\Api\Data\XOrder;


class XOrderItem extends \SM\Core\Api\Data\Contract\ApiDataAbstract {

    public function getName() {
        return $this->getData('name');
    }

    public function getId() {
        return $this->getData('product_id');
    }

    public function getTypeId() {
        return $this->getData('product_type');
    }

    public function getSku() {
        return $this->getData('sku');
    }

    public function getQtyOrdered() {
        return floatval($this->getData('qty_ordered'));
    }

    public function getQtyRefunded() {
        return floatval($this->getData('qty_refunded'));
    }

    public function getRowTotal() {
        return $this->getData('row_total');
    }

    public function getRowTotalInclTax() {
        return $this->getData('row_total_incl_tax');
    }

    public function getProductOptions() {
        $option         = [];
        $productOptions = $this->getData('product_options');
        if (isset($productOptions['options'])) {
            $option = array_merge($option, ['options' => $productOptions['options']]);
        }
        if (isset($productOptions['attributes_info'])) {
            $option = array_merge($option, ['attributes_info' => $productOptions['attributes_info']]);
        }
        if (isset($productOptions['bundle_selection_attributes'])) {
            $option = array_merge($option, unserialize($productOptions['bundle_selection_attributes']));
        }

        return $option;
    }

    public function getBuyRequest() {
        $buyRequest               = $this->getData('buy_request');
        $buyRequest['product_id'] = $this->getData('product_id');

        return $buyRequest;
    }

    public function getChildren() {
        return $this->getData('children');
    }

    public function getOriginImage() {
        return $this->getData('origin_image');
    }

    public function getIsChildrenCalculated() {
        return $this->getData('isChildrenCalculated');
    }
}